import React from 'react';
import { TextContext } from '../contexts/TextContext';


const SpeakButton = (props) => {


    return (
        <div className="speak-button">
            <button onClick={props.onClick}>Speak Text</button>
        </div>
    );
}

export default SpeakButton;
