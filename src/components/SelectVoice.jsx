import React, { useContext } from "react";
import { TextContext } from '../contexts/TextContext';

function SelectVoice(props) {
    const { setVoiceContext, voice, voices } = useContext(TextContext);

  const handleChange = (event) => {
    setVoiceContext(event.target.value);
  };

  console.log(voices)

  return voices?.length === 0 ? (
    <p>Loading...</p>
  ) : (
    <select onChange={handleChange} value={voice} class="voice-select">
      <option key={"selectone"} value={null}>Default voice</option>
      {voices?.map((voice, id) => (
        <>
          <option key={voice.name} value={id}>
            {voice.name}
          </option>
        </>
      ))}
    </select>
  );
}

export default SelectVoice;
