// create a text area that passes it's input to a react context

import React, { useState, useContext, useEffect } from 'react';
import { TextContext } from '../contexts/TextContext';

const TextArea = () => {
    const [text, setText] = useState('');
    const { setTextContext, text:textContext } = useContext(TextContext);

    useEffect(() => {
        setText(textContext);
    }, [textContext]);

    const handleChange = (e) => {
        setText(e.target.value);
        setTextContext(e.target.value);
    }

    // gradually reduce the font size of the text area as the text gets longer
    let fontSize = 4;
    if (text.length > 30) {
        fontSize = 3;
    }
    if (text.length > 55) {
        fontSize = 2;
    }
    if (text.length > 75) {
        fontSize = 1.5;
    }
    
    return (
        <div className='textarea'>
            <textarea placeholder='Tap here to start typing...' onChange={handleChange} value={text} className="text-area" style={{fontSize: fontSize + 'em'}} />
        </div>
    )
}

export default TextArea;