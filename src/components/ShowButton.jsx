import React from 'react';

const ShowButton = (props) => {
    return (
        <div className="show-button">
            <button onClick={props.onClick}>Show Text</button>
        </div>
    );
}
 
export default ShowButton;
