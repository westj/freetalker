import React, { useCallback, useEffect, useState } from "react";

const synth = window.speechSynthesis;

function setSpeech() {
    return new Promise(
        function (resolve, reject) {
            let synth = window.speechSynthesis;
            let id;

            id = setInterval(() => {
                if (synth.getVoices().length !== 0) {
                    resolve(synth.getVoices());
                    clearInterval(id);
                }
            }, 10);
        }
    )
}

const TextContext = React.createContext({
  text: "",
  voice: "",
  voices: [],
  setTextContext: () => {},
  clearTextContext: () => {},
  setVoiceContext: () => {},
  setVoicesContext: () => {},
  speakTextContext: () => {},
});

function TextContextProvider(props) {
  const [text, setText] = useState("");
  const [voice, setVoice] = useState("");
  const [voices, setVoices] = useState([]);

  const populateVoiceList = useCallback(() => {
    const newVoices = synth.getVoices();
    setVoices(newVoices);
    setVoicesContext(newVoices);
  }, []);

  useEffect(() => {
    function setSpeech() {
        return new Promise(
            function (resolve, reject) {
                let id;
    
                id = setInterval(() => {
                    if (synth.getVoices().length !== 0) {
                        resolve(synth.getVoices());
                        clearInterval(id);
                    }
                }, 10);
            }
        )
    }
    setSpeech().then(() => {
        populateVoiceList();
    })
    if (synth.onvoiceschanged !== undefined) {
      synth.onvoiceschanged = populateVoiceList;
    } 
  }, [populateVoiceList]);

  const setTextContext = (text) => {
    setText(text);
  };
  const clearTextContext = () => {
    setText("");
  };
  const setVoiceContext = (voice) => {
    setVoice(voice || 0);
  };
  const setVoicesContext = (voices) => {
    setVoices(voices);
  };
  const speakTextContext = () => {
    const utterance = new SpeechSynthesisUtterance(text);
    if (voice) {
      utterance.voice = synth.getVoices()[voice];
    }
    synth.speak(utterance);
    console.log(utterance)
  };
  return (
    <TextContext.Provider
      value={{
        text,
        voice,
        voices,
        setTextContext,
        clearTextContext,
        setVoiceContext,
        speakTextContext,
      }}
    >
      {props.children}
    </TextContext.Provider>
  );
}

export { TextContext, TextContextProvider };
