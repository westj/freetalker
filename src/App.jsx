import React, { useContext } from "react";
import { ShowButton, SpeakButton, TextArea } from "./components";
import { TextContextProvider, TextContext } from "./contexts/TextContext";
import SelectVoice from "./components/SelectVoice";
import "./App.css";

function App() {
  return (
    <div className="App">
      <TextContextProvider>
        <TextContext.Consumer>
          {({
            voice,
            voices,
            setVoiceContext,
            clearTextContext,
            speakTextContext,
          }) => (
            <div className="container">
              <SelectVoice props={{voice, voices, setVoiceContext}} />
              <TextArea />
              <button onClick={() => clearTextContext()} className="clear">Clear Text</button>
              <button onClick={() => speakTextContext()} className="speak">Speak Text</button>
            </div>
          )}
        </TextContext.Consumer>
      </TextContextProvider>
    </div>
  );
}

export default App;
